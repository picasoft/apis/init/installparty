# Api/casoft Init Jour 1 : Install party

Centralisation de quelques ressources pour l'installation d'une distribution Linux (ici Ubuntu 18.04), soit sur une machine virtuelle, soit en dual-boot Windows/MacOS.

Une chaîne d'intégration permet de construire automatiquement le PDF à partir du fichier .tex présent à la racine.

Après construction (uniquement sur la branche `master`), le document final est disponible [à cette adresse](https://uploads.picasoft.net/api/install_party.pdf).

Le chemin d'upload est défini dans le fichier [.gitlab-ci.yml](./gitlab-ci.yml).
